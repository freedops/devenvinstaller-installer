# DevEnvInstaller

# This is the installer for DevEnvInstaller
# An easy way to distribute a working build environment for a project.

# Installation


# This project requires docker, to install docker see
# http://docs.docker.io/installation/


# To run extract DevEnvInstaller.tar.gz and in that folder run ./setup <path_to_install_to> e.g. ./setup ~/home/devenvinst This will start the installation extraction process, and may take a little time. Once the environment has been set up a test build of a simple c++ program (cpp_test) this will create an installer called cpp_ubuntu.tar.gz. This can be tested in the same way. When the setup file for this is run it will build and then run the program, this program prints the following text:
 
#       Cpp_test compiled and running
# DevEnvInstaller has been successfully installed!
#  For more information go to www.freedops.org
# 

# Questions:

# Who is DevEnvInstaller for?

# Anyone who wants to make it easy for people to work on their software project

# How does it work?

# A project is set up by filling in a set of properties in a txt file (master_data). A script is then run that will build a docker container based on the properties file. When built, the script starts the container, and tests that a build occurs. If specified the script will also run a stated set of tests on the build.

# Okay, so how do I develop using the container?

# The code for a project is placed into a shared volume, therefore it can be modified, updated, committed in the normal way. To build/test the software the user can run a build that occurs in the container, using "./build_test", a script that is generated during setup.

# Which version control systems can I use?

# At the moment only git, but it is simple to add alternatives. There is a  second properties file (master_properties) where the commands for cloning and updating a project are set, another version control system can be added by adding appropriate entries for <versioncontrol>_git and <versioncontrol>_update. The value of 'version_control in master_data should be updated to <versioncontrol>.

# Which operating systems can I use?

# Similarly to the version control, at the moment only ubuntu, but again it is simple to add alternatives in the same way as for version control systems.

# Can I have some more information?

# Yes! Send an email to info@freedops.org

